How to build and install Q-Game:

1. Open the .sln file in Visual Studio.
2. Right click on your solution in Solution Explorer in Visual Studio and select Build. 
3. Go to the bin folder under your project folder and look for the EXE.
4. Copy KBishopAssignment2.exe to wherever you want to access it on your computer.
5. Double click the EXE to run it and play Q-Game.

LICENSE:
I chose to use the MIT license because it is very simple and it lets people do almost anything they want with the project, like making and distributing closed source versions.