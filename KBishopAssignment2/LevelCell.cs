﻿/*  LevelCell.cs 
*   This program allows for the creation of cell objects for the maze designer, and allows for change to the cell's tile image
*   Revision History: 
*   Kaeden Bishop, 2020.11.07: Created 
*   Kaeden Bishop, 2020.11.08: Finished 
*   Update
*/

using KBishopAssignment2.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KBishopAssignment2
{
    class LevelCell : PictureBox
    {
        public int row;
        public int column;
        public int cellType;

        public LevelCell(int row, int column, int cellType)
        {
            this.row = row;
            this.column = column;
            this.cellType = cellType;
            this.SizeMode = PictureBoxSizeMode.Zoom;
        }

        /// <summary>
        /// Method gets cellType passed to it
        /// Sets cellType of cell to what it was passed
        /// Changes the tile image of the cell to the cellType that has been passed
        /// </summary>
        /// <param name="cellType"></param>
        public void ChangeCellImage(int cellType)
        {
            this.cellType = cellType;
            switch (cellType)
            {
                case 0:
                    this.BackgroundImage = null;
                    break;
                case 1:
                    this.BackgroundImage = Resources.wallTile;
                    break;
                case 2:
                    this.BackgroundImage = Resources.redDoorTile;
                    break;
                case 3:
                    this.BackgroundImage = Resources.greenDoorTile;
                    break;
                case 4:
                    this.BackgroundImage = Resources.redBoxTile;
                    break;
                case 5:
                    this.BackgroundImage = Resources.greenBoxTile;
                    break;
                default:
                    MessageBox.Show("Something went wrong.");
                    break;
            }
            this.SizeMode = PictureBoxSizeMode.Zoom;
        }
    }
    }
