﻿/*  MazeDesignerForm.cs 
*   This program allows the user to generate a maze designer of a custom size and build a maze level using many different tiles.
*   Revision History: 
*   Kaeden Bishop, 2020.11.07: Created 
*   Kaeden Bishop, 2020.11.08: Finished 
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace KBishopAssignment2
{
    public partial class MazeDesignerForm : Form
    {
        public MazeDesignerForm()
        {
            InitializeComponent();
            
        }

        int rows, columns;
        int startX = 245;
        int startY = 135;
        int cellType = 0;
        List<LevelCell> cellList = new List<LevelCell>();
        int wallCounter;
        int doorCounter;
        int boxCounter;
        string fileName = "KBishopAssignment2Level1.txt";

        /// <summary>
        /// Gets a valid number of rows and columns from the user's input in textboxes
        /// Generates maze designer made of PictureBoxes with size based on entered rows and columns
        /// PictureBoxes are added to "cellList"
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                rows = Convert.ToInt32(txtRows.Text);
                columns = Convert.ToInt32(txtColumns.Text);

                if (rows <= 0 || columns <= 0)
                {
                    MessageBox.Show("Please enter valid data for rows and columns. (Must both be integers)");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please enter valid data for rows and columns. (Must both be greater than zero)");
            }

            for (int i = 0; i < rows; i++)
            {
                startY = 135;

                for (int j = 0; j < columns; j++)
                {
                    LevelCell c = new LevelCell(i, j, 0);
                    c.Left = startX;
                    c.Top = startY;
                    c.Height = 75;
                    c.Width = 75;

                    this.Controls.Add(c);
                    c.BackColor = Color.LightGray;
                    c.BorderStyle = BorderStyle.FixedSingle;
                    cellList.Add(c);

                    c.Click += new EventHandler(LevelCell_Click);

                    startY += 75;
                }
                startX += 75;
            }
        }

        /// <summary>
        /// Event Handlers for the toolbox tools
        /// Sets the cellType to whichever tool has been selected
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        #region Event Handlers  
       
        private void btnNone_Click(object sender, EventArgs e)
        {
            cellType = 0;
        }


        private void btnWall_Click(object sender, EventArgs e)
        {
            cellType = 1;
        }


        private void btnRedDoor_Click(object sender, EventArgs e)
        {
            cellType = 2;
        }


        private void btnGreenDoor_Click(object sender, EventArgs e)
        {
            cellType = 3;
        }


        private void btnRedBox_Click(object sender, EventArgs e)
        {
            cellType = 4;
        }


        private void btnGreenBox_Click(object sender, EventArgs e)
        {
            cellType = 5;
        }
        #endregion


        /// <summary>
        /// Number of walls, doors, and boxes in level are totaled up and displayed to the user in a MessageBox
        /// The maze design's numer of rows and columns are saved to text file KBishopAssignment2
        /// Each cell's row, column, and celltype are saved to text file KBishopAssignment2
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog SaveFileDialog = new SaveFileDialog();
            SaveFileDialog.FileName = fileName;
            SaveFileDialog.Filter = "Text File | *.txt";
            if (SaveFileDialog.ShowDialog() == DialogResult.OK)
            {

                StreamWriter writer = new StreamWriter(SaveFileDialog.OpenFile());

                writer.WriteLine(rows);
                writer.WriteLine(columns);

                foreach (LevelCell c in cellList)
                {
                    writer.WriteLine(c.row);
                    writer.WriteLine(c.column);
                    writer.WriteLine(c.cellType);
                }

                writer.Dispose();

                writer.Close();
            }

            foreach (LevelCell c in cellList)
            {
                if (c.cellType == 1)
                {
                    wallCounter++;
                }
                if (c.cellType == 2 || c.cellType == 3)
                {
                    doorCounter++;
                }
                if (c.cellType == 4 || c.cellType == 5)
                {
                    boxCounter++;
                }
            }

            MessageBox.Show("File saved successfully.\nTotal number of walls: " + wallCounter + "\nTotal number of doors: " + doorCounter + "\nTotal number of boxes: " + boxCounter);
        }

        /// <summary>
        /// Closes Maze Design Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MazeDesignerForm.ActiveForm.Close();
        }

        /// <summary>
        /// Handles event handlers for dynamically generated cells
        /// Calls the "ChangeCellImage" method for the selected cell and passes the current cellType
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LevelCell_Click(object sender, EventArgs e)
        {
            LevelCell c = sender as LevelCell;
            c.ChangeCellImage(cellType);
        }
    }
}

