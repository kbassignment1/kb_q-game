﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KBishopAssignment2
{
    public partial class ControlPanelForm : Form
    {
        public ControlPanelForm()
        {
            InitializeComponent();
        }

        private void btnDesign_Click(object sender, EventArgs e)
        {
            var designForm = new MazeDesignerForm();
            designForm.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            ControlPanelForm.ActiveForm.Close();
        }
    }
}
